package ejercicio;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class ejercicio1 {

	public static void main(String[] args) throws InterruptedException{
		System.setProperty("webdriver.chrome.driver","D:\\Chromedrivers\\crome\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		
		String url = "https://buggy.justtestit.org/register";
		driver.get(url);
		driver.manage().window().maximize();
		
		
		//Boton REGISTER
		driver.findElement(By.xpath("//a[@class=\"btn btn-success-outline\"]")).click();
		
		//Campo login
		driver.findElement(By.xpath("//input[@id=\"username\"]")).sendKeys("Prueba911");
		//Campo nombre
		driver.findElement(By.xpath("//input[@id=\"firstName\"]")).sendKeys("Nancy");
		//Campo apellido
		driver.findElement(By.xpath("//input[@id=\"lastName\"]")).sendKeys("Angeles");
		//Campo password
		driver.findElement(By.xpath("//input[@id=\"password\"]")).sendKeys("Prueba123%");
		//Campo confirmpass
		driver.findElement(By.xpath("//input[@id=\"confirmPassword\"]")).sendKeys("Prueba123%");
		
		//Boton Register
		driver.findElement(By.xpath("//button[@class=\"btn btn-default\"]")).click();
		
		Thread.sleep(5000);
		//Campo Usuario
		driver.findElement(By.xpath("//input[@ name=\"login\"]")).clear();
		driver.findElement(By.xpath("//input[@ name=\"login\"]")).sendKeys("Prueba911");
		
		//Campo Password
		driver.findElement(By.xpath("//input[@ name=\"password\"][1]")).clear();
		driver.findElement(By.xpath("//input[@ name=\"password\"][1]")).sendKeys("Prueba123%");
		
		//Boton login
		driver.findElement(By.xpath("//button[@ class=\"btn btn-success\"]")).click();
		System.out.println("LOG::::::::::::::: se loguea usuario");
		
		//Boton logout
		Thread.sleep(7000);
		driver.findElement(By.xpath("(//a[@ class=\"nav-link\"])[2]")).click();
		System.out.println("LOG::::::::::::::: se desloguea usuario");
		
		
		//Loguearse de nuevo
		//Campo Usuario
		Thread.sleep(5000);
		driver.findElement(By.xpath("//input[@ name=\"login\"]")).clear();
		driver.findElement(By.xpath("//input[@ name=\"login\"]")).sendKeys("Prueba911");
				
		//Campo Password
		driver.findElement(By.xpath("//input[@ name=\"password\"][1]")).clear();
		driver.findElement(By.xpath("//input[@ name=\"password\"][1]")).sendKeys("Prueba123%");
		
		//Boton login
		driver.findElement(By.xpath("//button[@ class=\"btn btn-success\"]")).click();
		System.out.println("LOG::::::::::::::: Ingresa de nuevo al sistema");
		
		//Boton perfil
		Thread.sleep(5000);
		driver.findElement(By.xpath("(//a[@ class=\"nav-link\"])[1]")).click();
		System.out.println("LOG::::::::::::::: Ingresa a pantalla perfil");
		
		Thread.sleep(5000);
		driver.findElement(By.xpath("//a[@ class=\"btn\"]")).click();
		System.out.println("LOG::::::::::::::: sale de perfil");
		
		Thread.sleep(5000);
		driver.findElement(By.xpath("(//img[@ class=\"img-fluid center-block\"])[2]")).click();
		System.out.println("LOG::::::::::::::: Ingresa al 2do auto");
		
		Thread.sleep(5000);
		driver.findElement(By.xpath("//textarea[@ id=\"comment\"]")).sendKeys("soy una prueba");
		System.out.println("LOG::::::::::::::: Ingresa comentario");
		
		Thread.sleep(3000);
		driver.findElement(By.xpath("//button [@ class=\"btn btn-success\"]")).click();
		System.out.println("LOG::::::::::::::: Boton vota");
		
		
	}

}
